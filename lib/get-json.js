const axios     = require('axios');
const cheerio   = require('cheerio');
var fs          = require("fs")

const url = 'https://about.gitlab.com/handbook/values';

let valuesText = ['Collaboration', 'Results', 'Efficiency', 'Diversity &amp; Inclusion', 'Iteration', 'Transparency']
let values = [];
/*
[
    {
        value: '',
        text: '',
        subvalues: [
            title: '',
            text: ''
        ]
    }
]
*/

axios
    .get(url)
    .then(res => {
        const $ = cheerio.load(res.data);
        $('h3').each((i, elm) => {
            checkElm($, elm);
        })

        //console.log(values);
    })
    .catch(error => {
        console.error(error);
    });



function checkElm($, elm) {
    const thisHeader = $(elm).html();
    const thisIndex = valuesText.indexOf(thisHeader);
    if (thisIndex > -1) {

        let nextUntil = $(elm).nextUntil('h3');
        if (thisIndex == valuesText.length - 1) {
            nextUntil = $(elm).nextUntil('h2');
        }

        values.push({
            value: thisHeader,
            text: nextUntil.html(),
            subvalues: []
        })

        $(nextUntil).each((j, item) => {
            subValue($, j, item, thisHeader);
        })
        
        console.log(values);
        fs.writeFileSync("./out/values.json", JSON.stringify(values, null, 2))
    }
}

function subValue($, j, item, header) {
    const thisItem = $(item);
    const thisVIndex = values.findIndex(x => x.value === header);

    if (thisItem.get(0).tagName == 'h5') {
        values[thisVIndex].subvalues.push({
            title: thisItem.html(),
            text: thisItem.nextUntil('h5').html()
        })
    }
}